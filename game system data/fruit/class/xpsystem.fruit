include "modules/core/audio/audio.fruit"
include "rogue/game system data/fruit/ai/npctypes.fruit"
include "rogue/game system data/fruit/elite/eliterank.fruit"
include "rogue/skillscript/skillscript.fruit"
include "rogue/game system data/fruit/class/accolade.fruit"
include "rogue/game system data/fruit/currencies/currencydefinition.fruit"

enum XPTypes
{
	NormalXP
	DarkZoneXP
	BooWingMedicalXP
	BooWingSecurityXP
	BooWingTechXP
}

class XPToLevelFormula < tooltip="XPToLevel = myXPRequirementBase + (myXPRequirementPerLevel * playerLevel) + (myXPRequirementPerLevelSQ * playerLevel^2)" >
{
	int myMinLevelRange 1
	int myMaxLevelRange 1
	float myXPRequirementPerLevelSQ 0.0
	float myXPRequirementPerLevel	300.0
	float myXPRequirementBase		0.0
}

list XPToLevelFormulaList
{
	XPToLevelFormula
}

class LevelUpCurrencyReward
{
	int myMinLevelRange 1
	int myMaxLevelRange 1
	CurrencyReferenceList myCurrencyRewards
}

list LevelUpCurrencyRewardsList
{
	LevelUpCurrencyReward
}

class XPToLevelFormulaData
{
	LevelUpCurrencyRewardsList myLevelUpCurrencyRewards
	XPToLevelFormulaList myXPToLevelFormulas < tooltip="XPToLevel = myXPRequirementBase + (myXPRequirementPerLevel * playerLevel) + (myXPRequirementPerLevelSQ * (playerLevel * playerLevel))" >
	XPTypes myXPType NormalXP
	int myMaxLevel 1
}

list XPToLevelFormulaLists
{
	XPToLevelFormulaData
}

class AgentKillXpFormula
{
	float myAgentXPPerLevelSQ 	0.0
	float myAgentXPPerLevel 	100.0
	float myAgentXPBase 		0.0
}

class LevelDifferenceModifier < tooltip="Will be added to XP so FinalXP = BaseKillXP + (BaseKillXP * LevelDiffModifier)" >
{
	int myMinLevelDifference 1
	int myMaxLevelDifference 1
	float myModifier 0
}

list LevelDifferenceModifierList
{
	LevelDifferenceModifier
}

class GroupBonus < tooltip="Modifier added to XP, so FinalXP = BaseKillXP + (BaseKillXP * GroupBonus)" >
{
	float my1MembersModifier 0
	float my2MembersModifier 0
	float my3MembersModifier 0.166
	float my4MembersModifier 0.3
}

class ArchetypeData
{
	NPCArchetype myArchetype Unset
	float myBaseXPModifier 0 < tooltip="Bonus = AgentKillXP * BaseXPModifier" >
	bool myIsSpecial FALSE < tooltip="TRUE if killing this NPC gives a Special Kill accolade" >
}

list ArchetypeDataList
{
	ArchetypeData
}

class EliteData
{
	EliteRank* myEliteRank
	float myBaseXPModifier 0 < tooltip="Bonus = AgentKillXP * BaseXPModifier" >
}

list EliteDataList
{
	EliteData
}

class DarkZoneBonus < tooltip="Modifier added to XP, so FinalXP = BaseKillXP + (BaseKillXP * DarkZoneBonus)" >
{
	float myModifier 0
}

class KillXpToSkillXpFormula
{
	float myBaseXP							1.0
	float myLevelDifferenceConstant			1.0
	float mySkillLevelDifferenceConstant 	1.0
	float myEliteNPCConstant				1.0
	int myDifferenceCap						0
}

class KillXpToSkillXpFormulaOverride
{
	SkillScript* mySkillScript
	KillXpToSkillXpFormula myFormula
}

list KillXpToSkillXpFormulaOverrideList
{
	KillXpToSkillXpFormulaOverride
}

class KillXpToSkillXpFormulaList
{
	KillXpToSkillXpFormula myDefault
	KillXpToSkillXpFormulaOverrideList myOverrides
}

class XpToSkillLevelFormula
{
	float myBaseXP 1.0
	float mySkillLevelConstant 1.0
}

class XpToSkillLevelFormulaOverride
{
	SkillScript* mySkillScript
	XpToSkillLevelFormula myFormula
}

list XpToSkillLevelFormulaOverrideList
{
	XpToSkillLevelFormulaOverride
}

class XpToSkillLevelFormulaList
{
	XpToSkillLevelFormula myDefault
	XpToSkillLevelFormulaOverrideList myOverrides
}

class SkillXPFormulas
{
	KillXpToSkillXpFormulaList myKillXpToSkillXpFormulae < tooltip="SkillXP = myBaseXP * myLevelDifferenceConstant^(VictimLevel - AgentLevel) * myEliteNPCConstant * mySkillLevelDifferenceConstant^( ( AgentLevel - SkillAcquiredLevel ) / SkillLevel )  (capped to 0 when difference is below myDifferenceCap)" >
	XpToSkillLevelFormulaList myXpToSkillLevelFormulae < tooltip="XPToLevel = myBaseXP * mySkillLevelConstant^SkillLevel" >
}

list RogueStageBonusXPList
{
	float
}

class XPSystem
{
	XPToLevelFormulaLists myXPToLevelFormulas < tooltip="XPToLevel = myXPRequirementBase + (myXPRequirementPerLevel * playerLevel) + (myXPRequirementPerLevelSQ * (playerLevel * playerLevel))" >
	AgentKillXpFormula myAgentKillXpFormula < tooltip="AgentKillXP = myAgentXPPerLevelSQ*agentLV^2 + myAgentXPPerLevel*agentLV + myAgentXPBase" >
	AgentKillXpFormula myAgentKillXpFormulaDZ < tooltip="AgentKillXP = myAgentXPPerLevelSQ*agentLV^2 + myAgentXPPerLevel*agentLV + myAgentXPBase" >
	AgentKillXpFormula myPVPKillXpFormula < tooltip="PlayerKillXP = myAgentXPPerLevelSQ*agentLV^2 + myAgentXPPerLevel*agentLV + myAgentXPBase" >
	AgentKillXpFormula myPVPBountySurviveXPFormula < tooltip="PlayerBountySyrviveXP = myAgentXPPerLevelSQ*agentLV^2 + myAgentXPPerLevel*agentLV + myAgentXPBase" >

	XPToLevelFormulaLists myCompanionXPToLevelFormulas < tooltip="XPToLevel = myXPRequirementBase + (myXPRequirementPerLevel * playerLevel) + (myXPRequirementPerLevelSQ * (playerLevel * playerLevel))" >

	LevelDifferenceModifierList myLevelDifferenceModifiers < tooltip="Final XP = (AgentKillXP + Bonuses) * Modifier" >
	GroupBonus myGroupBonus < tooltip="Bonus = (AgentKillXP * Modifier) - AgentKillXP" >
	ArchetypeDataList myArchetypeData
	EliteDataList myEliteData
	DarkZoneBonus myDarkZoneBonus < tooltip="Bonus = (AgentKillXP * Modifier) - AgentKillXP" >
	SkillXPFormulas mySkillXPFormulas
	RogueStageBonusXPList myRogueStageBonusXPList
	float myMinRogueAgentDamageFraction 0 < tooltip="Minimum damage (fraction of rogue agent's health) dealt to receive share of DZ XP for rogue kill" >
	AccoladeList myAccolades
	AccoladeSettings myAccoladeSettings
}
