// Must match enum declared in RShared_ThrowableItemTypes.h
enum ThrowableItemBallisticTypeEnum
{
	None	
	Grenade	
	SkillProxy
	SkillProxyBig
	SkillProxySticky
	FirstAid_Base
	FirstAid_LightWeight
	Stickybomb
	Turret
	SupportStation
	Armadillo
}

// Must match enum declared in RShared_ThrowableItemConstantData.h
enum ThrowableBallisticPresetMode
{
	Default
	LowCover
	LowCornerCover
	LowBackCover
	LowBackCornerCover
	HighCover
	HighCornerCover
	HighBackCover
	HighBackCornerCover
}

class ThrowableBallisticPreset
{
	ThrowableBallisticPresetMode myPresetMode Default
	string myAngleOffsetCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 -0, -13.3333 -2, -2.66667 -6.33333, }, outTangents = { 13.3333 2, 2.66667 6.33333, 0 0, }, properties = { 203, 2250, 1226, }, values = { -30 10, 10 16, 18 35, }, }, maxValueBounds = 180, minValueBounds = -180, numKeys = 3, }" < bhv=inputgraph >	
	string mySpeedCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 -0, -13.3333 -6.33333, -2.66667 3, }, outTangents = { 13.3333 6.33333, 2.66667 -3, 0 0, }, properties = { 203, 1227, 2250, }, values = { -30 6, 10 25, 18 16, }, }, maxValueBounds = 40, minValueBounds = 0, numKeys = 3, } " < bhv=inputgraph >	
	bool myLinkThrowAngleAndSpeedCurve TRUE // Should the speed to read based on the angle offset curve
	float myMaxAngle 22.0
	float myMinAngle -30.0
	bool myLockArcAtMinMax false
	bool myBlend false
	float myBlendTime 0.2
}

list ThrowableBallisticPresetList
{
	ThrowableBallisticPreset
}

class ThrowableBallisticConfig < uid=auto >
{
	ThrowableItemBallisticTypeEnum myBallisticType None
	int myMaxBounces 3
	vec3f mySpinFactor 0.02;0.015;0.01
	float myBlendOverPercentage 100.0
	float myMinPathTimeForExtraSpin 0.8
	float myUIBounceMinSpeed 0.2
	float myUIBounceMinDistanceToTarget 0.2
	float myGroundBounceRestitution 0.6
	float myWallBounceRestitution 0.6
	float myRadius 0.1
	float myGroundCollisionNormalY 0.9
	float myRestCollisionNormalY 0.2
	float myInitalRadiusPercentage 10.0
	int myRadiusAdjustSegmentCount 3
	bool myShouldLandOnNavMesh false
	bool myOrientateOnEnd TRUE
	bool myOffsetStartPointByAttachmentPoint FALSE
	bool myUseCOG FALSE
	bool mySnapToUp TRUE
	bool myDEBUGUseOldThrowAngle TRUE
	bool myCanThrowBeBlocked FALSE
	ThrowableBallisticPresetList myPresets
}

class ThrowableBallisticGlobalParams
{	
	float				mySimulationStepTime 0.1
	float				myMaxSimulationTime 10.0
	int					myMaxSimulationSteps 100
	float				myMinXZVelocity 0.4
	float				myMinYVelocity 0.3

	string				myReleaseAngleAnimationVariableName ""
	string				myReleaseBoneName ""
	string				myCOGBoneName ""
	string				myAttachBoneName ""
}
