include "rogue/game system data/fruit/ai/noncombatantalertlevels.fruit"
include "rogue/game system data/fruit/ai/detectionsoundtypes.fruit"
include "rogue/game system data/fruit/faction/faction.fruit"
include "rogue/game system data/fruit/ai/npctypes.fruit"

class AlertLevelValues
{
	float myThreshold 0.0 < tooltip="Detection Value threshold for the transition to next alert level" >
	int myPauseBeforeValueDecay 0 < tooltip="The delay time (milliseconds) before the detection value starts to decay" >
	float myDecayRate 0.0 < tooltip="Rate of decay of the detection value per second" >
	int myIncreaseDelay 0 < tooltip="The delay time (milliseconds) before the alert level can increase" >
	float myInheritThreatRange 0.0 < min=0.0 max=100.0 tooltip="The range in which the alert level can be inherited" >
	int myInheritThreatDuration 0 < min=0 tooltip="This alert level can be inherited by other agents after the last stimulus duration this duration (milliseconds)" >
	float myInheritThreatWeight 1 < min=0 tooltip="When an alert level is inherited, a random between the source alert level to the target alert level is done with this weight" >
}


class AlertLevelDetectionRange
{
	float myMaxDistance 0.0 < min=0.0 max=100.0 tooltip="Detection max distance for this alert level" >
	NonCombatantAlertLevel myAlertLevel < tooltip="The alert level to enter into when the NPC detects the stimulus" >
}

list AlertLevelDetectionRangeList
{
	AlertLevelDetectionRange
}

class VisionAlertLevelDetectionRange : AlertLevelDetectionRange
{
	float myMinSpeed 0.0 < min=0.0 tooltip="The seen NPC's minimum speed to enter the given alert level" >
}

list VisionAlertLevelDetectionRangeList
{
	VisionAlertLevelDetectionRange
}


class SoundAlertLevels
{
	SoundType mySoundType < tooltip="The type of sound that triggers these alert levels" >
	AlertLevelDetectionRangeList myAlertLevelRanges < tooltip="The alert levels with their corresponding max distance" >
}

list SoundAlertLevelsList
{
	SoundAlertLevels
}


class VisionAlertLevels
{
	FactionRelationType myFactionRelation < tooltip="The faction relation that triggers these alert levels" >
	VisionAlertLevelDetectionRangeList myAlertLevelRanges < tooltip="The alert levels with their corresponding max distance and min speed" >
}

list VisionAlertLevelsList
{
	VisionAlertLevels
}

class AimedAtAlertLevels
{
	FactionRelationType myFactionRelation < tooltip="The faction relation that triggers these alert levels" >
	AlertLevelDetectionRangeList myAlertLevelRanges < tooltip="The alert levels with their corresponding max distance and min speed" >
}

list AimedAtAlertLevelsList
{
	AimedAtAlertLevels
}


list NPCArchetypesList
{
	NPCArchetype
}


class NonCombatantDetectionProfile
{
	AlertLevelValues myAwareValues < tooltip="Detection values for the Aware level" >
	AlertLevelValues myConcernedValues < tooltip="Detection values for the Concerned level" >
	AlertLevelValues myScaredValues < tooltip="Detection values for the Scared level" >
	AlertLevelValues myPanickedValues < tooltip="Detection values for the Panicked level" >

	SoundAlertLevelsList mySoundAlertLevels	< tooltip="The alert levels for sound stimuli" >
	VisionAlertLevelsList myVisionAlertLevels < tooltip="The alert levels for vision stimuli" >
	AimedAtAlertLevelsList myAimedAtAlertLevels < tooltip="The alert levels for aimed at stimuli" >
	float myAimedAtMaxRadius < tooltip="The max radius in meters where the NPC receives aimed at stimuli" >
	NonCombatantAlertLevel myShotAtAlertLevel Unknown < tooltip="The alert level for shot at stimuli" >
	NonCombatantAlertLevel myExitSequenceMaxAlertLevel Unknown < tooltip="The maximum alert level for stimuli when exiting a sequence" >
	bool myDisableVisionDuringInteraction false < tooltip="Disable vision stimuli when the agent tries to interact with a player, used by civilians only" >
	FactionRelationType myDisableVisionDuringInteractionFactionRelation Neutral < tooltip="The faction relation to ignore vision stimuli from when interacting, usually the faction relation towards players" >
	int myDisableVisionAfterInteractionDuration 0 < min=0 tooltip="The delay time (milliseconds) before the civilian can change his alert level by seeing an agent who could interact with him" >
	bool myVisionRequiresLOS true < tooltip="Wether vision stimuli require a Line Of Sight check" >
	bool myAlertZoneVisionRequiresLOS true < tooltip="Wether vision stimuli in the alert zone require a Line Of Sight check" >
	
	int myMaxBullyTime 15000 < tooltip="The time (milliseconds) bullying will last unless deactivated through the EndBullying node" >
	int myBullyCooldown 20000 < tooltip="The delay time (milliseconds) after bullying (or being bullied) before looking for a new bully target" >
	int myRecoveryTime 10000 < tooltip="The delay time (milliseconds) during which we stay unavailable as a target after being bullied." >
	bool myIsBully true < tooltip="Is that agent a bully" >
	
	bool myInheritThreat false < tooltip="Can that agent inherit a threat and alert level from another agent (of same archetype)." >
	NonCombatantAlertLevel myLowestInheritedAlertLevel Scared < tooltip="Lowest alert level of seen agent that will be inherited." >
	int myInheritThreatDelay 2000 < tooltip="Time (milliseconds) in sight required before inheriting threat" >
	NPCArchetypesList myInheritThreatArchetypes
}
