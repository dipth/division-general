// %1 mousekb, %2 gamepad, %3 ps4 %4+ command and arguments

console.cmd cmd="ui.input.forcemousekb %1"
console.cmd cmd="ui.input.forcegamepad %2"
console.cmd cmd="UI.Input.ForceGamepadPS4 %3"

script.wait time=1

script.exec blocking=true filename="%4+"
